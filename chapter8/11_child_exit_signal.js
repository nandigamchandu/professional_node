var spawn = require('child_process').spawn

var child = spawn('sleep', ['5'])
setTimeout(()=>{
    child.kill()
}, 2000)

child.on('exit', (code, signal)=>{
    if(code){
        console.log('child process terminated with code ' + code)
    }else if (signal){
        console.log('child process terminated because of sigal ' + signal)
    }
})