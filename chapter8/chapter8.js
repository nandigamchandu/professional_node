var child_process = require('child_process')
var exec = child_process.exec
exec('cat *.js | wc -l', (err, stdout, stderr)=>{
    if(err){
        console.log('child process exited with error code', err.code)
        return 
    }
    console.log(stdout)
} )

var env = process.env,
varName,
envCopy = {};

for (varName in env){
    envCopy[varName] = env[varName]
}

envCopy['CUSTOM ENV VAR'] = 'some value'
envCopy['CUSTOM ENV VAR 2'] = 'some other value'

exec('ls -la', {env: envCopy}, (err, stdout, stderr) =>{
    if (err){throw err}
    console.log('stdout: ', stdout)
    console.log('stderr: ', stderr)
})

const node = "/home/nandigamchandu/.nvm/versions/node/v11.12.0/bin/node";
exec(
  `${node} child.js`,
  { env: { number: 123 } },
  (err, stdout, stderr) => {
    if (err) {
      throw err;
    }
    console.log("stdout: \n", stdout);
    console.log("stdout: \n", stderr);
  }
);

// Creating the child process
var spawn = require('child_process').spawn
var child = spawn("tail", ["-f", "/home/nandigamchandu/house.txt"]);

child.stdout.resume()
child.stdout.on('data', function (data) {
    console.log('tail output: ' + data);
});
child.stderr.on('data', function (data) {
    console.log('tail error output: ' + data);
});

// Sending data to the Child process

