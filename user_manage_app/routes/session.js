var users = require("../data/users");
var notLoggedIn = require("./middleware/not_logged_in");
var express = require("express");
var User = require("../data/models/user");
var passport = require("passport");
var router = express.Router();
var auth = require("./middleware/auth")

router.get("/", (req, res) => {
  res.render("session/user", { title: "Login" });
});

router.get("/session/new", (req, res) => {
  res.render("session/new", { title: "Log in" });
});

router.post("/session", (req, res, next) => {
  return passport.authenticate("local", (err, passportUser, info) => {
    if (err) {
      return next(err);
    }
    console.log(passportUser)
    if (passportUser) {
      const user = passportUser;
      user.token = passportUser.generateJWT();
      console.log({ "token": "Token" + " " + user.toAuthJSON().token })
      return res.json({ user: user.toAuthJSON() });
    }
    return res.status(400).send(info);
  })(req, res, next);
});

router.delete("/session", (req, res, next) => {
  req.session.destroy();
  res.redirect("/users");
});

module.exports = router;
