const mongoose = require("mongoose");


const TaskSchema = new mongoose.Schema({
    createdBy: {type: String, required: true},
    task: {type: String, required: true},
    createdAt: {
        type: Date,
        default: Date.now
    },
    AssignTo:{
        type:String,
    }

})

module.exports = TaskSchema