var mongoose = require("mongoose");
var request = require("request");
var crypto = require('crypto')
var jwt = require('jsonwebtoken')

const validate_18_years_old_or_more = date => {
  return Date.now() - date.getTime() > 18;
};

const filterTwitterHandle = (handle)=>{
  if(!handle){
    return 
  }
  handle = handle.trim()
  if(handle.indexOf('@')=== 0){
    handle = handle.substring(1)
  }
  return handle
}

var emailRegexp = /.+\@.+\..+/;
var UserSchema = new mongoose.Schema({
  username: { type: String, unique: true },
  name: mongoose.Schema.Types.Mixed,
  hash: String,
  salt: String,
  email: {
    type: String,
    required: true,
    match: emailRegexp,
    unique: true
  },
  gender: {
    type: String,
    required: true,
    uppercase: true,
    enum: ["M", "F"]
  },
  birthday: {
    type: Date,
    validate: [validate_18_years_old_or_more, "You must be 18 year old or more"]
  },

  phone: {
    type: String,
    validate: {
      isAsync: true,
      validator: function(v, cb) {
        setTimeout(function() {
          var phoneRegex = /\d{3}-\d{3}-\d{4}/;
          var msg = v + " is not a valid phone number!";
          cb(phoneRegex.test(v), msg);
        }, 5);
      },
      message: "Default error message"
    }
  },

  twitter:{
    type: String,
    validate:{
      isAsync: true,
      validator: function(v, cb){
        request("https://twitter.com/"+v, function(err, res){
          if(err){
            return cb(false, "no user")
          }
          if(res.statusCode > 299){
            cb(false, "no user")
          }else{
            cb(true)
          }
        });
      }
    },
    set: filterTwitterHandle,
    get: filterTwitterHandle,
  },

  bio: String,
  meta: {
    created_at: {
      type: Date,
      default: Date.now
    },
    updated_at: {
      type: Date,
      default: Date.now
    }
  }
});

UserSchema.virtual("twitter_url").get(function(){
  if (this.twitter) {
    return "http://twitter.com/" + this.twitter;
  }
});

UserSchema.virtual('full_name').get(function(){
  if(typeof this.name === 'string'){
    return this.name
  }
  return [this.name.first, this.name.last].join(' ')
}).set(function(fullName){
  var nameComponents = fullName.split(' ')
  this.name = { last: nameComponents.pop(),
    first: nameComponents.join(' ')} 
})

UserSchema.methods.setPassword = function (password) {
  this.salt = crypto.randomBytes(16).toString('hex')
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex')
}

UserSchema.methods.validatePassword = function (password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex')
  return this.hash === hash
}

UserSchema.methods.generateJWT = function () {
  const today = new Date()
  const expirationDate = new Date(today)
  expirationDate.setDate(today.getDate() + 60)
  return jwt.sign({
    email: this.email,
    id: this._id,
    exp: parseInt(expirationDate.getTime() / 1000, 10)
  }, 'secret')
}

UserSchema.methods.toAuthJSON = function () {
  return {
    _id: this._id,
    email: this.email,
    token: this.generateJWT()
  }
}

module.exports = UserSchema;
