var oneSecond = 1000 * 1
// setTimeout(function(){
//     document.write('<p>Hello there.</p>')
// }, oneSecond)
// setInterval(function(){
//     document.write('<p>Hello there.</p>')
// }, oneSecond)

var timeout_ms = 2000
var timeout = setTimeout(function(){
    console.log("timed out!")
}, timeout_ms)
// console.log(timeout)
clearTimeout(timeout)

var interval = setInterval(function (){
    console.log("tick")
}, 1000)

clearInterval(interval)


// Blocking the Event Loop

// process.nextTick(function nextTick1(){
//     var a= 0
//     while(true){
//         a ++
//     }
// })

process.nextTick(function nextTick2(){
    console.log("time out!")
})

setTimeout(function timeout(){
    console.log("timeOut!")
}, 1000)

console.log("=========================================")