function add(){
    var sum = 0;
    for (let i of arguments){
        sum += i
    }
    return sum
}

console.log(add(2, 3, 4, 5))

// substituting the use of arguments array through Rest parameter syntax

function avg(...args){
    var sum = 0
    for (let value of args){
        sum  += value
    }
    return sum / args.length
}

console.log(avg(2, 3, 4, 5))

function avgArray(arr){
    var sum = 0 
    for(let i=0, j = arr.length; i < j; i++){
        sum += arr[i]
    }
    return sum / arr.length
}

console.log(avgArray([2, 3, 4, 5]))

// Using apply method functions are objects
console.log(avg.apply(null, [2, 3, 4, 5]))
// using spread operator
console.log(avg(...[2, 3, 4, 5]))

// JavaScript uses functions as classes.
function makePerson(first, last){
    return{
        first: first,
        last: last
    }
}

function personFullName(person){
    return person.first + " " + person.last
}

function personFullNameReversed(person){
    return person.last + ' '+ person.first
}

var s = makePerson('Chandu', 'Nandigam')
console.log(personFullName(s))
console.log(personFullNameReversed(s))

function makePerson1(first, last){
    return{
        first: first,
        last: last,
        fullName: function(){
            return this.first + ' ' + this.last
        },
        fullNameReversed: function(){
            return this.last + ' ' + this.first
        }
    }
}

var s = makePerson1('chandu', 'namdigam')
console.log(s.fullName())
console.log(s.fullNameReversed())

var s = makePerson1('Simon', 'willison')
var fullName = s.fullName
console.log(fullName())

// We can take advantage of the this keyword to improve our makePerson function
// Functions that are designed to be called by new are called constructor functions
function Person(first, last){
    this.first = first
    this.last = last
    this.fullName  = function(){
        return this.first + ' ' + this.last
    }
    this.fullNameReversed = function(){
        return this.last + ' ' + this.first
    }
}

var s = new Person('chandu', "namdigam")
console.log(s.fullName())
console.log(s.fullNameReversed())

// s.fistNameCaps() TypeError: s.fistNameCaps is not a function
Person.prototype.firstNameCaps = function(){
    return this.first.toUpperCase()
}
console.log(s.firstNameCaps())

var o = {}
Person.apply(o, ["william", "orange"])
console.log(o.fullName())

// inner Function
function parentFunc(){
    var a = 1
    function nestedFunc(){
        var b = 4
        return a + b
    }
    return nestedFunc()
}

console.log(parentFunc())

//  Closures
function makeAdder(a){
    return function(b){
        return a + b
    }
}

var add5 = makeAdder(5)
var add20 = makeAdder(20)
console.log(add5(6))
console.log(add20(7))