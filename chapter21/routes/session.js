var users = require('../data/users')
var notLoggedIn = require('./middleware/not_logged_in')
var express = require("express");
var router = express.Router();


router.get('/session/new', notLoggedIn, (req, res)=>{
    res.render('session/new', {title: "Log in"})
})

router.post('/session', notLoggedIn, (req, res)=>{
    if (users[req.body.username] && 
        users[req.body.username].password === req.body.password){
        req.session.user = users[req.body.username]
        res.redirect('/users')
    }else{
        res.redirect('/session/new')
    }
})

router.delete('/session', (req, res, next)=>{
    req.session.destroy()
    res.redirect('/users')
})

module.exports = router;