var connect = require("connect");

var saveRequest = require('./07_save_request')
var writeHeader = require("./05_write_header");
var replyText = require("./03_reply_text");

var app = connect();
app.use(writeHeader("X-Powered-By", "Node"))
app.use(saveRequest(__dirname + '/requests'))
app.use(replyText("Hello World"));
app.listen(8080);
