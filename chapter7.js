// manipulating file path
var path = require('path')
var fs = require('fs')
// normalizing path
console.log(path.normalize('/foo/bar//baz/asdf/quux/..'))
// joining path
var path1 = path.join('/foo', 'bar', 'baz/asdf', 'quux', '.')
console.log(path1)
// resolving paths
console.log(path.resolve('/foo/bar', './baz'))
console.log(path.resolve('/foo/bar', '/tmp/file/'))
console.log(path.resolve('foo/bar', './tmp/file/'))
// Finding the Relative path between two absolute paths
console.log(path.relative('/data/orandea/test/aaa', '/data/orandea/imp1/bbb'))

// Extracting Components of a Path
console.log(path.dirname('/foo/bar/baz/test.txt'))
console.log(path.dirname('/foo/bar/baz/'))
console.log(path.basename('/foo/bar/baz/test.txt'))
console.log(path.basename('/foo/bar/baz/test.txt', '.txt'))
console.log(path.basename('/foo/bar/baz/test.txt', '.html'))

// determine the extension.
console.log(path.extname('/a/b/index.html'))
console.log(path.extname('/a/b/index.'))
console.log(path.extname('/a/b/index'))
console.log(path.extname('/a/b/.'))

// Determining the Existance of a path
console.log(
    fs.exists('/home/nandigamchandu/practice', function(exists){
        console.log('exists:', exists)
    })
)

// Introducing the FS module
// Querying file statistics
fs.stat("/home/nandigamchandu/.zshrc", function(err, stats){
    if(err){ throw err}
    console.log(stats)
    console.log(stats.isFile())
    console.log(stats.isDirectory())
});

// Reading From a file
fs.open("/home/nandigamchandu/house.txt", 'r', function(err, fd){
    if (err){throw err}
    var readBuffer = new Buffer.alloc(1024),
    bufferOffset = 0,
    bufferLength = readBuffer.length,
    filePosition = 100;
    fs.read(fd,
        readBuffer,
        bufferOffset,
        bufferLength,
        filePosition,
        function read(err, readBytes){
            if (err){throw err}
            console.log('just read ' + readBytes + ' bytes')
            if (readBytes > 0){
                console.log(readBuffer.slice(0, readBytes).toString())
            }
        })
})

// Writing to file
fs.open('./my_file.txt', 'a', function opened(err, fd){
    if(err){throw err}
    var writeBuffer = new Buffer.from('writing this string'),
    bufferPosition = 0,
    bufferLength = writeBuffer.length,
    filePosition = null;
    fs.write(fd,
        writeBuffer,
        bufferPosition,
        bufferLength,
        filePosition,
        (err, written)=>{
            if(err){throw err}
            console.log('wrote ' + written + ' bytes')
        })

})

// closing a file
function openAndWriteToSystemLog(writeBuffer, callback){
    fs.open('./my_file1.txt', 'a', function open(err, fd){
        if (err){return callback(err)}
        function notifyError(err){
            fs.close(fd, function(){
                callback(err)
            })
        }
        var bufferOffset = 0,
        bufferLength = writeBuffer.length,
        filePosition = null;
        fs.write(fd, writeBuffer, bufferOffset, bufferLength, filePosition,
            function wrote(err, written){
                if (err){return notifyError(err)}
                fs.close(fd, function(){
                    callback(err)
                })
            })
    })
}

openAndWriteToSystemLog(new Buffer.from("My system log"),
(err)=>{
    if(err){
        console.log("error while opening and writing:", err.message)
        return
    }
    console.log("All done with no error")
})