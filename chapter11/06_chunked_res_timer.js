require('http').createServer((req, res)=>{
    res.writeHead(200, {'Content-Type': 'text/plain'})
    var left = 10
    var interval = setInterval(()=>{
        for(var i = 0; i< 100; i++){
            res.write(Date.now() + " ")
        }
        console.log(`before ${left}`)
        if(-- left===0){
            console.log(`after \n ${left}`)
            clearInterval(interval)
            res.end()
        }
    }, 1000)
}).listen(4000)