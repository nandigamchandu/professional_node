require('net').createServer((socket) => {
    socket.setTimeout(10000)
    socket.on('data', (data) => { console.log(data) })
    socket.setKeepAlive(true)
    socket.on('timeout', () => {
        socket.write('idle timeout, disconnecting, bye!')
    })
    
}).listen(4001)