var express = require("express");
// var parser = require("xml2json");
var router = express.Router();
var notLoggedIn = require("./middleware/not_logged_in");
var loadUser = require("./middleware/load_user");
var restrictUserToSelf = require("./middleware/restrictUserToSelf");
var User = require("../data/models/user");
var async = require("async");
var request = require("request")
var path = require('path')
var fs = require('fs')

router.get("/users", (req, res, next) => {
  var page = (req.query.page && parseInt(req.query.page)) || 0;
  async.parallel(
    [
      next => {
        User.find().estimatedDocumentCount(next);
      },
      next => {
        User.find({})
          .sort({ name: 1 })
          .skip(page * 3)
          .limit(3)
          .exec(next);
      },
    ],
    (err, results) => {
      if (err) {
        return next(err);
      }

      var count = results[0];
      var users = results[1];

      var lastPage = (page + 1) * 3 >= count;
      res.render("users/index", {
        title: "Users",
        users: users,
        page: page,
        lastPage: lastPage
      });
    }
  );
});

router.get("/users/new", (req, res) => {
  res.render("users/new", { title: "New User" });
});

router.get("/users/:name", loadUser, function(req, res, next) {
  res.render("users/profile", { title: "User profile", user: req.user });
});

router.post("/users", (req, res, next) => {
  User.create(req.body, err => {
    if (err) {
      if (err.code === 11000) {
        res.send("Conflict", 409);
      } else {
        next(err);
      }
      return;
    }
    res.redirect("/users");
  });
});

router.delete("/users/:name",notLoggedIn, loadUser, (req, res, next) => {
  req.user.remove(err => {
    if (err) {
      return next(err);
    }
    res.redirect("/users");
  });
});

router.get("/get_csv", (req, res)=>{
  // request(
  //   "https://book.integration2.testaroom.com/api/properties.xml?api_key=c017604d-72c1-5414-adde-a18b77add9e7&auth_token=1c042bb6-913b-5a64-ad85-2e720dbca5bc", (err, res1, body)=>{
  //     if(err){
  //       console.error(err)
  //     }
  //     console.log(body)
  //     // var res1_json = parser.toJson(body);
  //     res.end("get result");

  //   });
  file1 = path.normalize("test.xml"); 
  request("https://book.integration2.testaroom.com/api/properties.xml?api_key=c017604d-72c1-5414-adde-a18b77add9e7&auth_token=1c042bb6-913b-5a64-ad85-2e720dbca5bc").pipe(fs.createWriteStream(file1))
  
  res.end("file is saved")
})
module.exports = router;