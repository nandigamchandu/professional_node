var mongoose = require("mongoose");
var request = require("request");
const validate_18_years_old_or_more = date => {
  return Date.now() - date.getTime() > 18;
};

const filterTwitterHandle = (handle)=>{
  if(!handle){
    return 
  }
  handle = handle.trim()
  if(handle.indexOf('@')=== 0){
    handle = handle.substring(1)
  }
  return handle
}

var emailRegexp = /.+\@.+\..+/;
var UserSchema = new mongoose.Schema({
  username: { type: String, unique: true },
  name: mongoose.Schema.Types.Mixed,
  password: String,
  email: {
    type: String,
    required: true,
    match: emailRegexp
  },
  gender: {
    type: String,
    required: true,
    uppercase: true,
    enum: ["M", "F"]
  },
  birthday: {
    type: Date,
    validate: [validate_18_years_old_or_more, "You must be 18 year old or more"]
  },

  phone: {
    type: String,
    validate: {
      isAsync: true,
      validator: function(v, cb) {
        setTimeout(function() {
          var phoneRegex = /\d{3}-\d{3}-\d{4}/;
          var msg = v + " is not a valid phone number!";
          cb(phoneRegex.test(v), msg);
        }, 5);
      },
      message: "Default error message"
    }
  },

  twitter:{
    type: String,
    validate:{
      isAsync: true,
      validator: function(v, cb){
        request("https://twitter.com/"+v, function(err, res){
          if(err){
            return cb(false, "no user")
          }
          if(res.statusCode > 299){
            cb(false, "no user")
          }else{
            cb(true)
          }
        });
      }
    },
    set: filterTwitterHandle,
    get: filterTwitterHandle,


  },

  bio: String,
  meta: {
    created_at: {
      type: Date,
      default: Date.now
    },
    updated_at: {
      type: Date,
      default: Date.now
    }
  }
});

UserSchema.virtual("twitter_url").get(function(){
  if (this.twitter) {
    return "http://twitter.com/" + this.twitter;
  }
});

UserSchema.virtual('full_name').get(function(){
  if(typeof this.name === 'string'){
    return this.name
  }
  return [this.name.first, this.name.last].join(' ')
}).set(function(fullName){
  var nameComponents = fullName.split(' ')
  this.name = { last: nameComponents.pop(),
    first: nameComponents.join(' ')} 
})

module.exports = UserSchema;
