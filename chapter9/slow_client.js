const fs = require('fs')
const path =
  "/home/nandigamchandu/practice/web/javascript/nodePractice/chapter9/sample.txt";

require('http').createServer(function(req, res){
    var rs = fs.createReadStream(path)

    rs.on('data', (data)=>{
        if(!res.write(data)){
            rs.pause()
        }else{
            console.log("data is flushed on kernal")
        }
    })

    res.on('drain', ()=>{
        rs.resume()
    })

    rs.on('end', ()=>{
        res.end(' end ')
    })
}).listen(8080)